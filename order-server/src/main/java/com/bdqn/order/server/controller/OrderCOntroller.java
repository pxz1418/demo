package com.bdqn.order.server.controller;

import com.bdqn.order.server.config.StudentConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope
public class OrderCOntroller {
    @Autowired
    private StudentConfig studentConfig;
    @GetMapping("/getInfo")
    public String getInfo(){
        return studentConfig.getName()+":"+studentConfig.getAge();
    }
}
