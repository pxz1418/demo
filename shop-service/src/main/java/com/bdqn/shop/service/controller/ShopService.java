package com.bdqn.shop.service.controller;

import com.bdqn.shop.service.service.ShopChannel;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class ShopService {
    @Resource(name = ShopChannel.SHOP_OUTPUT)
    private MessageChannel messageChannel;
    @GetMapping("/sendMsg/{content}")
    public String sendShopMessage(@PathVariable("content") String content){
        boolean isSendSuccess = messageChannel.
                send(MessageBuilder.withPayload(content).build());
        return isSendSuccess ? "发送成功" : "发送失败";
    }
    @StreamListener(ShopChannel.SHOP_INPUT)
    public void receive(String message){
        System.out.println(message);
    }
}
