package com.bdqn.order.server.consumer.feign;

import com.bdqn.service.api.dto.OrderDTO;
import org.springframework.stereotype.Component;

@Component
public class FailedFeignServiceImpl implements FeignService {

    @Override
    public OrderDTO getOrderById(Integer orderId) throws InterruptedException {
        return null;
    }

    @Override
    public OrderDTO getErrorOrder() {
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setName("服务中失败的订单");
        orderDTO.setId(-1);
        return orderDTO;
    }
}
