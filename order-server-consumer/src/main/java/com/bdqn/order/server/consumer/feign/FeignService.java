package com.bdqn.order.server.consumer.feign;

import com.bdqn.service.api.api.IOrderService;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.context.annotation.Primary;

@Primary
@FeignClient(value = "ORDER-SERVER",fallback = FailedFeignServiceImpl.class)
public interface FeignService extends IOrderService{
}
