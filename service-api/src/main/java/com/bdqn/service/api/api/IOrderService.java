package com.bdqn.service.api.api;

import com.bdqn.service.api.dto.OrderDTO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

public interface IOrderService {
    @GetMapping("/orderId/{orderId}")
    OrderDTO getOrderById(@PathVariable("orderId")Integer orderId) throws InterruptedException;
    @GetMapping("/errorOrder")
    OrderDTO getErrorOrder();
}
